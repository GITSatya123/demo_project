package com.st.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.st.bean.Student;


public class Test {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context=null;
		Student p1=null;
		
		
		context= new AnnotationConfigApplicationContext();
		context.scan("com.st");
		context.refresh();
		
		p1=context.getBean("student",Student.class);
			System.out.println(p1);
	}

}
